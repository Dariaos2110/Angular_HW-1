import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {AppComponent} from './app.component';
import {ListComponent} from './Components/list-component/list.component';
import {FormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {PokemonInfoComponent} from './Components/pokemon-info/pokemon-info.component';
import {HttpClientModule} from '@angular/common/http';
import {NavbarComponent} from './Components/navbar/navbar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatFormFieldModule} from '@angular/material/form-field';
import {MatPaginatorModule} from '@angular/material/paginator';

@NgModule({
  declarations: [
    AppComponent,
    ListComponent,
    PokemonInfoComponent,
    NavbarComponent
  ],
  imports: [
    RouterModule.forRoot([
        {path: '', pathMatch: 'full', component: ListComponent},
        {path: 'pokemons', component: ListComponent},
        {path: 'pokemons/:pokemonId', component: PokemonInfoComponent},
        {path: 'caughtPokemons', component: ListComponent},
        {path: 'freePokemons', component: ListComponent},
      ]
    ),
    BrowserModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatPaginatorModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
