import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Pokemon} from './Components/pokemon-item/Pokemon';

@Injectable({
  providedIn: 'root'
})
export class PokemonService {

  constructor(
    private http: HttpClient,
  ) {
    this.getFreePokemons = this.getFreePokemons.bind(this);
    this.getCaughtPokemons = this.getCaughtPokemons.bind(this);
    this.getPokemons = this.getPokemons.bind(this);
    this.releasePokemon = this.releasePokemon.bind(this);
    this.catchPokemon = this.catchPokemon.bind(this);
    this.getItemsCount = this.getItemsCount.bind(this);
  }

  getPokemons(page: number, limit: number): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons?_page=${page}&_limit=${limit}`);
  }

  getPokemon(id: number): Observable<Pokemon> {
    return this.http.get<Pokemon>(`http://localhost:3000/pokemons/${id}`);
  }

  getCaughtPokemons(page: number, limit: number): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons?isCaught=true&_page=${page}&_limit=${limit}`);
  }

  getFreePokemons(page: number, limit: number): Observable<Pokemon[]> {
    return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons?isCaught=false&_page=${page}&_limit=${limit}`);
  }

  getItemsCount(pathName: string): Observable<Pokemon[]> {
    switch (pathName) {
      case 'pokemons':
        return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons`);
      case 'caughtPokemons':
        return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons?isCaught=true`);
      case 'freePokemons':
        return this.http.get<Pokemon[]>(`http://localhost:3000/pokemons?isCaught=false`);
    }
  }

  catchPokemon(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.put<Pokemon>(`http://localhost:3000/pokemons/${pokemon.id}`, {
      ...pokemon,
      damage: (pokemon.damage ?? 0) + 10, isCaught: true,
      dateOfCapture: new Date().toUTCString()
    });
  }

  releasePokemon(pokemon: Pokemon): Observable<Pokemon> {
    return this.http.put<Pokemon>(`http://localhost:3000/pokemons`, {
      ...pokemon,
      isCaught: false,
      dateOfCapture: null
    });
  }
}
