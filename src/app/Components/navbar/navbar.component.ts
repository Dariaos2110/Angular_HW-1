import { Component, OnInit } from '@angular/core';
import {PokemonService} from '../../pokemon.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

  constructor(private PokemonsService: PokemonService, private route: ActivatedRoute) { }

  ngOnInit(): void {
  }

}
