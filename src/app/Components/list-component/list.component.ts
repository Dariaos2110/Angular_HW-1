import {Component, OnInit} from '@angular/core';
import {Pokemon} from '../pokemon-item/Pokemon';
import {PokemonService} from '../../pokemon.service';
import {ActivatedRoute} from '@angular/router';
import {MatPaginatorModule, PageEvent} from '@angular/material/paginator';


@Component({
  selector: 'app-list-component',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css']
})

export class ListComponent implements OnInit {

  pokemons: Pokemon[] = [];
  pathName = 'pokemons';

  constructor(private PokemonsService: PokemonService, private route: ActivatedRoute) {
  }

  methods = {
    freePokemons: this.PokemonsService.getFreePokemons,
    caughtPokemons: this.PokemonsService.getCaughtPokemons,
    pokemons: this.PokemonsService.getPokemons,
  };
  class = 'gallery';
  classItem = 'gallery_item';
  length = 200;
  pageSize = 10;
  pageIndex = 1;
  pageSizeOptions: number[] = [5, 10, 30, 50];
  pageEvent: PageEvent;

  ngOnInit(): void {
    this.getDataSource(this.pageIndex, this.pageSize);
    this.PokemonsService.getItemsCount(this.pathName).subscribe((result: Pokemon[]) => this.length = result.length);
  }

  getDataSource(page: number, limit: number): any {
    this.pathName = this.route.toString().split(', path:')[1].slice(1, -2) ?? 'pokemons';
    this.methods[this.pathName](page, limit).subscribe((pokemons: Pokemon[]) => {
      this.pokemons = pokemons;
      this.pageSize = limit;
      this.pageIndex = page;
    });
  }

  onClickSwitch(): void {
    this.class = this.class === 'list' ? 'gallery' : 'list';
    this.classItem = this.class + '_item';
  }

  onClickBtn(pokemon: Pokemon): void {
    pokemon.isCaught ? this.PokemonsService.releasePokemon(pokemon)
        .subscribe()
      : this.PokemonsService.catchPokemon(pokemon)
        .subscribe();
    this.getDataSource(this.pageIndex, this.pageSize);
  }
}
