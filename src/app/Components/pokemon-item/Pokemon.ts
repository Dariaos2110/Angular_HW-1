export class Pokemon {
  id: number;
  name: string;
  damage: number;
  isCaught?: boolean;
  dateOfCapture?: Date | null;
}
